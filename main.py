# find quadratic roots ax^2 + bx + c = 0
import math
from typing import Tuple, Optional


def solution(a, b, c) -> Optional[Tuple[float, float]]:
    result = None
    d2 = (b * b) - (4 * a * c)

    if d2 == 0:
        result = -b / (2 * a),

    elif d2 > 0:
        result = ((-b - math.sqrt(d2)) / (2 * a), ((-b + math.sqrt(d2)) / (2 * a)))

    return result


# print(solution(a=1, b=4, c=4))
if __name__ == "__main__":
    assert solution(1, 6, 5) == (-5, -1)
    assert solution(1, 4, 4) == (-2,)
    assert solution(1, 6, 45) is None
